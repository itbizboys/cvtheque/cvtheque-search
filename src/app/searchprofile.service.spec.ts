import { TestBed } from '@angular/core/testing';

import { SearchprofileService } from './searchprofile.service';

describe('SearchprofileService', () => {
  let service: SearchprofileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchprofileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
