import { Component, OnDestroy, OnInit } from '@angular/core';

import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Rx';
import { Person } from './person';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
   list = [
     { id: 1, name: 'ayoub' },
     { id: 2, name: 'abdellah' }, 
     { id: 3, name: 'othman' }];

    constructor() { }
    // ngOnInit() {
    // }

    dtOptions: DataTables.Settings = {};

    ngOnInit(): void {
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 2
      };
    }

}

// export class HomeComponent implements OnDestroy, OnInit {
//   dtOptions: DataTables.Settings = {};
//   persons: Person[] = [];
//   // We use this trigger because fetching the list of persons can be quite long,
//   // thus we ensure the data is fetched before rendering
//   dtTrigger: Subject<any> = new Subject();

//   constructor(private http: Http) { }

//   ngOnInit(): void {
//     this.dtOptions = {
//       pagingType: 'full_numbers',
//       pageLength: 2
//     };
//     this.http.get('data/data.json')
//       .map(this.extractData)
//       .subscribe(persons => {
//         this.persons = persons;
//         // Calling the DT trigger to manually render the table
//         this.dtTrigger.next();
//       });
//   }

//   ngOnDestroy(): void {
//     // Do not forget to unsubscribe the event
//     this.dtTrigger.unsubscribe();
//   }

//   private extractData(res: Response) {
//     const body = res.json();
//     return body.data || {};
//   }
// }