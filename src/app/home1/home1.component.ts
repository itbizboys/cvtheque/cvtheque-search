import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchprofileService } from '../searchprofile.service';
import { MatTableDataSource } from '@angular/material/table';
import { Profile } from 'src/profile'
import { MatPaginator } from '@angular/material/paginator';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ELEMENT_DATA : Profile[];
  displayedColumns: string[] = ['sender', 'city', 'post', 'attachment'];
  dataSource = new MatTableDataSource<Profile>(this.ELEMENT_DATA)
  clickMessage = '';
  profileForm = new FormGroup({
    villeInput: new FormControl(''),
    profileInput: new FormControl(''),
  })
  searchQuery = "";
  expControl = new FormControl();
  langControl = new FormControl();

get villeInput(): AbstractControl { // name property
    return this.profileForm.get('villeInput')
}

get profileInput(): AbstractControl { // name property
  return this.profileForm.get('profileInput')
}

get expInput(): AbstractControl { // name property
  return this.expControl.value
}

get langInput(): AbstractControl { // name property
  return this.langControl.value
}
  constructor(private service:SearchprofileService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator
    // this.getAllProfiles()
    // this.getfProfiles()
  }

  public getAllProfiles(){
    // let resp = this.service.profileSearch();
    
    let resp = this.service.fprofileSearch("ville=casablanca&post=ingenieur");
    resp.subscribe(profile=>this.dataSource.data=profile as Profile[])
  }

  public getfProfiles(){
    let resp = this.service.fprofileSearch("ville=casablanca&post=ingenieur");
    resp.subscribe(profile=>this.dataSource.data=profile as Profile[])
  }

  onClickMe() {
    let resp;
    if(this.villeInput.value){
      this.searchQuery = "ville=" + this.villeInput.value
    }
    if(this.profileInput.value){
      this.searchQuery = this.searchQuery + "&post=" + this.profileInput.value
    }
    if(this.expInput){
      this.searchQuery = this.searchQuery + "&experience=" + this.expInput
    }
    if(this.langInput){
      this.searchQuery = this.searchQuery + "&language=" + this.langInput
    }
    console.log(this.searchQuery);

    if(this.searchQuery){
      resp = this.service.fprofileSearch(this.searchQuery);
    } else {
      resp = this.service.profileSearch();
    }
    
    resp.subscribe(profile=>this.dataSource.data=profile as Profile[])
    this.searchQuery = "";
    this.expControl.setValue(null)
    this.langControl.setValue(null)

  }
}
