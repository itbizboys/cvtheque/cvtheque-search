import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// class Person {
//   sender: string;
//   city: string;
//   post: string;
// }

@Injectable({
  providedIn: 'root'
})
export class SearchprofileService {

  constructor(private http:HttpClient) { }

  /**
   * profileSearch
   */
  public profileSearch() {
    return this.http.get("http://5.189.157.89:3000/profiles");
  }

  /**
   * fprofileSearch
   */
  public fprofileSearch(test: string) {
    
    // return this.http.get("http://localhost:3000/fprofiles?ville=casablanca&post=ingenieur");
    return this.http.get("http://5.189.157.89:3000/fprofiles?" + test);
  }
}
