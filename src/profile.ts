export interface Profile {
    sender: string;
    city: string;
    post: string;
    filename: string;
  }